# Vector operations

Implemented basic operations with [vectors](https://www.mathsisfun.com/algebra/vectors.html).

## dependencies

```shell
npm install
```

## tests

```shell
npm run tests
```
