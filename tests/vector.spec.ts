import { Vector } from '../vector';

describe('Basic operations with Vector', () => {
  let v1: Vector;
  let v2: Vector;
  let v3: Vector;

  beforeEach(async () => {
    v1 = new Vector(1, 2, 3);
    v2 = new Vector(4, 5);
    v3 = new Vector(6, 7, 8, 9);
  });

  it('v1 + v2 + v3', () => expect(v1.add(v2, v3).coordinates).toEqual([11, 14, 11, 9]));

  it('v1 - v2', () => expect(v1.sub(v2).coordinates).toEqual([-3, -3, 3]));

  it('|v1| (magnitude)', () => expect(+v1.len.toFixed(5)).toEqual(3.74166));

  it('v1 * 2', () => expect(v1.mul(2).coordinates).toEqual([2, 4, 6]));

  it('v1 / 2', () => expect(v1.div(2).coordinates).toEqual([0.5, 1, 1.5]));

  it('v1 · v2', () => expect(v1.dot(v2)).toEqual(14));

  it('v1 × v2', () => expect(v1.cross(v2).coordinates).toEqual([-15, 12, -3]));
});
