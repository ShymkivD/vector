export class Vector {
  coordinates: number[];

  constructor(...coordinates: number[]) {
    this.coordinates = coordinates;
  }

  add(...v: Vector[]) {
    v.push(this);
    const coordinates = [];
    const len = Math.max(...v.map((v) => v.coordinates.length));

    for (let i = 0; i < len; i++) {
      coordinates[i] = v.map((v) => v.coordinates[i] || 0).reduce((a, b) => a + b);
    }

    return new Vector(...coordinates);
  }

  inv(): Vector {
    return this.mul(-1);
  }

  sub(v2: Vector) {
    return this.add(v2.inv());
  }

  mul(a: number) {
    return new Vector(...this.coordinates.map((c) => c * a));
  }

  div(a: number) {
    return this.mul(1 / a);
  }

  get len(): number {
    return Math.hypot(...this.coordinates);
  }

  dot(...v: Vector[]): number {
    v.push(this);
    const coordinates = [];
    const len = Math.max(...v.map((v) => v.coordinates.length));

    for (let i = 0; i < len; i++) {
      coordinates[i] = v.map((v) => v.coordinates[i] || 0).reduce((a, b) => a * b);
    }

    return coordinates.reduce((a, b) => a + b);
  }

  // only for 3D vectors
  cross({ coordinates: v2 }: Vector) {
    const v1 = [...this.coordinates];
    for (const index of [...Array(3).keys()]) {
      if (!v1[index]) {
        v1[index] = 0;
      }
      if (!v2[index]) {
        v2[index] = 0;
      }
    }

    return new Vector(
      v1[1] * v2[2] - v1[2] * v2[1],
      v1[2] * v2[0] - v1[0] * v2[2],
      v1[0] * v2[1] - v1[1] * v2[0],
    );
  }
}
